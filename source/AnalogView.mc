import Toybox.Graphics;
import Toybox.Lang;
import Toybox.Math;
import Toybox.System;
import Toybox.Time;
import Toybox.Time.Gregorian;
import Toybox.WatchUi;

class AnalogView extends WatchUi.WatchFace {
  private var _isAwake as Boolean?;
  private var _screenShape as ScreenShape;
  private var _dndIcon as BitmapResource?;
  private var _offscreenBuffer as BufferedBitmap?;
  private var _dateBuffer as BufferedBitmap?;
  private var _screenCenterPoint as Array<Number>?;
  private var _fullScreenRefresh as Boolean;
  private var _partialUpdatesAllowed as Boolean;
  private var _TAU as Float;

  public function initialize() {
    WatchFace.initialize();
    _screenShape = System.getDeviceSettings().screenShape;
    _fullScreenRefresh = true;
    _partialUpdatesAllowed = (WatchUi.WatchFace has :onPartialUpdate);
    _TAU = 2 * Math.PI;
  }

  public function onLayout(dc as Dc) as Void {

    if (Graphics has :BufferedBitmap) {
      _offscreenBuffer = new Graphics.BufferedBitmap({
          :width=>dc.getWidth(),
          :height=>dc.getHeight(),
      });

      _dateBuffer = new Graphics.BufferedBitmap({
          :width=>dc.getWidth(),
          :height=>Graphics.getFontHeight(Graphics.FONT_MEDIUM)
          });
    } else {
      _offscreenBuffer = null;
    }

    _screenCenterPoint = [dc.getWidth() / 2, dc.getHeight() / 2] as Array<Number>;
  }

  private function generateHandPoints(points as Array< Array<Float> >, angle as Float) {
    var angleX = Math.cos(angle);
    var angleY = Math.sin(angle);
    var centerPoint = _screenCenterPoint;
    var result = new Array< Array<Float> >[points.size()];
    for (var i = 0; i < points.size(); i++) {
      var x = (points[i][0] * angleX) - (points[i][1] * angleY);
      var y = (points[i][0] * angleY) + (points[i][1] * angleX);

      result[i] = [centerPoint[0] + x, centerPoint[1] + y] as Array<Float>;
    }

    return result;
  }

  private function drawHand(points as Array< Array<Float> >, dc as Dc) {
    dc.setPenWidth(3);
    dc.setAntiAlias(true);
    for (var i = 0; i < points.size() - 1; i++) {
      dc.drawLine(points[i][0], points[i][1], points[i+1][0], points[i+1][1]);
    }
    dc.fillPolygon(points.slice(2, 5));
    dc.setAntiAlias(false);
  }

  private function drawHourHand(dc as Dc) {
    var points = [
      [5, -6],
      [10, -10],
      [5, -60],
      [0, -65],
      [-5, -60],
      [-10, -10],
      [-5, -6],
    ];
    var clockTime = System.getClockTime();
    var hourHandAngle = ((((clockTime.hour % 12) * 60) + clockTime.min) / (12 * 60.0)) * _TAU;
    var result = generateHandPoints(points, hourHandAngle);
    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
    drawHand(result, dc);
  }

  private function drawMinuteHand(dc as Dc) {
    var points = [
      [4, -6],
      [10, -10],
      [5, -90],
      [0, -105],
      [-5, -90],
      [-10, -10],
      [-4, -6],
    ];
    var clockTime = System.getClockTime();
    var minuteHandAngle = _TAU / 60 * clockTime.min;
    var result = generateHandPoints(points, minuteHandAngle);
    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
    drawHand(result, dc);
  }

  private function drawSecondHand(dc as Dc) {
    var width = dc.getWidth();
    var height = dc.getHeight();
    dc.setPenWidth(3);
    dc.setColor(Graphics.COLOR_DK_BLUE, Graphics.COLOR_TRANSPARENT);
    dc.drawCircle(width / 2, height / 2, 7);

    var clockTime = System.getClockTime();
    var secondHandAngle = (clockTime.sec / 60.0) * _TAU;

    var handWidth = 1;
    var handLength = (dc.getWidth() / 2) - 5;
    var handPoints = [
      [handWidth, -6],
      [handWidth, -handLength],
      [-handWidth, -handLength],
      [-handWidth, -6],
    ];
    var handResult = generateHandPoints(handPoints, secondHandAngle);

    var tailLength = 20;
    var tailPoints = [
      [handWidth, 6],
      [handWidth, tailLength],
      [-handWidth, tailLength],
      [-handWidth, 6],
    ];
    var tailResult = generateHandPoints(tailPoints, secondHandAngle);

    // TODO: Clipping has to be broken I see a ton of issues with the second
    // hand extending where its filling the polygon without another explanation
    // var curClip = getBoundingBox(tailResult);
    // var bBoxWidth = curClip[1][0] - curClip[0][0] + 2;
    // var bBoxHeight = curClip[1][1] - curClip[0][1] + 2;
    // dc.setClip(curClip[0][0], curClip[0][1], bBoxWidth, bBoxHeight);

    dc.setColor(Graphics.COLOR_DK_BLUE, Graphics.COLOR_BLACK);
    dc.fillPolygon(handResult);
    dc.fillPolygon(tailResult);
  }

  // private function getBoundingBox(points as Array< Array<Number or Float> >) as Array< Array<Number or Float> > {
  //   var min = [9999, 9999] as Array<Number>;
  //   var max = [0,0] as Array<Number>;

  //   for (var i = 0; i < points.size(); ++i) {
  //     if (points[i][0] < min[0]) {
  //       min[0] = points[i][0];
  //     }

  //     if (points[i][1] < min[1]) {
  //       min[1] = points[i][1];
  //     }

  //     if (points[i][0] > max[0]) {
  //       max[0] = points[i][0];
  //     }

  //     if (points[i][1] > max[1]) {
  //       max[1] = points[i][1];
  //     }
  //   }

  //   return [min, max] as Array< Array<Number or Float> >;
  // }

  //! Draws the clock tick marks around the outside edges of the screen.
  //! @param dc Device context
  private function drawHashMarks(dc as Dc) as Void {
    var width = dc.getWidth();
    var height = dc.getHeight();

    var outerRad = width / 2;
    var smallInnerRad = outerRad - 4;
    var bigInnerRad = outerRad - 10;

    dc.setAntiAlias(true);
    var deg6 = Math.PI / 30;
    for (var i = 0; i <= 59; i += 1) {
      var deg = deg6 * i;
      if (i % 5 == 0) {
        var sY = outerRad + bigInnerRad * Math.sin(deg);
        var eY = outerRad + outerRad * Math.sin(deg);
        var sX = outerRad + bigInnerRad * Math.cos(deg);
        var eX = outerRad + outerRad * Math.cos(deg);
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.setPenWidth(3);
        dc.drawLine(sX, sY, eX, eY);
      } else {
        var sY = outerRad + smallInnerRad * Math.sin(deg);
        var eY = outerRad + outerRad * Math.sin(deg);
        var sX = outerRad + smallInnerRad * Math.cos(deg);
        var eX = outerRad + outerRad * Math.cos(deg);
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.setPenWidth(1);
        dc.drawLine(sX, sY, eX, eY);
      }
    }
    dc.setAntiAlias(false);
  }

  private function drawBatteryPercentage(dc as Dc) as Void {
    var batteryPercentage = System.getSystemStats().battery;
    if (batteryPercentage <= 15) {
      dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
    } else {
      dc.setColor(Graphics.COLOR_DK_BLUE, Graphics.COLOR_TRANSPARENT);
    }
    dc.setPenWidth(10);
    dc.drawArc(
      _screenCenterPoint[0],
      _screenCenterPoint[1],
      (dc.getWidth() / 2) - 5,
      Graphics.ARC_CLOCKWISE,
      180,
      180 - batteryPercentage / 100 * 90
    );

  }

  public function onUpdate(dc as Dc) as Void {
    var targetDc = null;

    _fullScreenRefresh = true;
    var offscreenBuffer = _offscreenBuffer;
    if (null != offscreenBuffer) {
      dc.clearClip();
      targetDc = offscreenBuffer.getDc();
    } else {
      targetDc = dc;
    }

    var width = targetDc.getWidth();
    var height = targetDc.getHeight();

    targetDc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_WHITE);
    targetDc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());

    drawBatteryPercentage(targetDc);
    drawHashMarks(targetDc);
    $.hrHistory(targetDc);
    drawHourHand(targetDc);
    drawMinuteHand(targetDc);

    // Draw the arbor
    targetDc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
    targetDc.fillCircle(width / 2, height / 2, 2);

    var dateBuffer = _dateBuffer;
    if ((null != dateBuffer) && (null != offscreenBuffer)) {
      var dateDc = dateBuffer.getDc();
      dateDc.drawBitmap(0, -(height / 4), offscreenBuffer);
      drawDateString(dateDc, width / 2, 0);
    }

    drawBackground(dc);

    if (_partialUpdatesAllowed) {
      onPartialUpdate(dc);
    }

    _fullScreenRefresh = false;
  }

  private function drawDateString(dc as Dc, x as Number, y as Number) as Void {
    var info = Gregorian.info(Time.now(), Time.FORMAT_LONG);
    var dateStr = Lang.format("$1$ $2$ $3$", [info.day_of_week, info.month, info.day]);

    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
    dc.drawText(x, y, Graphics.FONT_MEDIUM, dateStr, Graphics.TEXT_JUSTIFY_CENTER);
  }

  public function onPartialUpdate(dc as Dc) as Void {
    if (!_fullScreenRefresh) {
      drawBackground(dc);
    }

    drawSecondHand(dc);
  }

  private function drawBackground(dc as Dc) as Void {
    var width = dc.getWidth();
    var height = dc.getHeight();

    var offscreenBuffer = _offscreenBuffer;
    if (null != offscreenBuffer) {
      dc.drawBitmap(0, 0, offscreenBuffer);
    }

    var dateBuffer = _dateBuffer;
    if (null != dateBuffer) {
      dc.drawBitmap(0, height / 4, dateBuffer);
    } else {
      drawDateString(dc, width / 2, height / 4);
    }
  }

  public function onEnterSleep() as Void {
    _isAwake = false;
    WatchUi.requestUpdate();
  }

  public function onExitSleep() as Void {
    _isAwake = true;
  }

  public function turnPartialUpdatesOff() as Void {
    _partialUpdatesAllowed = false;
  }
}

class AnalogDelegate extends WatchUi.WatchFaceDelegate {
  private var _view as AnalogView;

  public function initialize(view as AnalogView) {
    WatchFaceDelegate.initialize();
    _view = view;
  }

  public function onPowerBudgetExceeded(powerInfo as WatchFacePowerInfo) as Void {
    System.println("Average execution time: " + powerInfo.executionTimeAverage);
    System.println("Allowed execution time: " + powerInfo.executionTimeLimit);
    _view.turnPartialUpdatesOff();
  }
}
